package dbconection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class InsertarAlumnos {
    public static void main(String[] args) {
        System.out.println("Hola");
        //Conectarnos a la base de datos e insertar un alumno
        //Nos vamos a conectar a la base de datos
        //1. Obtener conexion
        //2. Preparar la consulta SQL
        //3  Ejecutar la consulta SQL
        

        //-----------------------        
        //-----------------------        
        //-----------------------        
        //1. Obtener conexion
        //1.1 Los datos (Usuario, Clave, host)
        // host: dónde está la base de datos, nombre de la base de datos, puerto de conexión
        Connection miConexion = null;
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/ciencias_nahuel"; //Aca tenemos que poner lo que nos diga el fabricante de la base de datos

        try {

            miConexion = DriverManager.getConnection(host, usuario, clave);
            //2.Preparar la consulta SQL para insertar
            String consultaSQL = "INSERT INTO `alumnos` (`nombre`, `apellido`, `email`, `asistencia`) VALUES ('Jorge', 'Lopez', 'jorge@gmail.com', '1');";
            PreparedStatement sqlPreparado = miConexion.prepareStatement(consultaSQL);

            //3  Ejecutar la consulta SQL
            //ResultSet miResultado = sqlPreparado.executeQuery();
            sqlPreparado.execute();
            

            System.out.println("Insertaste el usuario, chabon! :) ");
        } catch (SQLException miError) {
            System.out.println("Tene cuidado con la configuracion de la base ;) ");
            System.out.println(miError);
        } finally {
            if (miConexion != null){
                try {
                    miConexion.close();
                } catch (SQLException ex) {
                    System.out.println("Error al cerrar la base de datos.");
                    System.out.println(ex);
                }
            }
            
        }
        
        System.out.println("Chau");
    }
}
