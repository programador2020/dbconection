package dbconection;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbConection {

    public static void main(String[] args) {
        System.out.println("Hola");
        //Nos vamos a conectar a la base de datos
        //1. Obtener conexion
        //2. Preparar la consulta SQL
        //3  Ejecutar la consulta SQL
        //4. Mostrar los resultados

        //-----------------------        
        //-----------------------        
        //-----------------------        
        //1. Obtener conexion
        //1.1 Los datos (Usuario, Clave, host)
        // host: dónde está la base de datos, nombre de la base de datos, puerto de conexión
        Connection miConexion = null;
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/ciencias_nahuel"; //Aca tenemos que poner lo que nos diga el fabricante de la base de datos

        try {

            miConexion = DriverManager.getConnection(host, usuario, clave);
            //2.Preparar la consulta SQL
            String consultaSQL = "SELECT * FROM materias";
            PreparedStatement sqlPreparado = miConexion.prepareStatement(consultaSQL);

            //3  Ejecutar la consulta SQL
            ResultSet miResultado = sqlPreparado.executeQuery();
            //4. Mostrar los resultados
            while (miResultado.next()) {
                System.out.println(miResultado.getString("horario"));
            }

            System.out.println("Se va ejecutando bien...");
        } catch (SQLException miError) {
            System.out.println("Tene cuidado con la configuracion de la base ;) ");
            System.out.println(miError);
        } finally {
            if (miConexion != null){
                try {
                    miConexion.close();
                } catch (SQLException ex) {
                    System.out.println("Error al cerrar la base de datos.");
                    System.out.println(ex);
                }
            }
            
        }

        System.out.println("Chau");
    }

}
